<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Validator;

class ProjectController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function all()
    {
        return response()->json(Project::where('user_id', '=', $this->user->id), 200);
    }

    public function show($id)
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'          => 'required',
            'description'   => 'max:255'
        ]);

        if($validator->fails()){
            return response()->json([
                'error'     => $validator->errors()
            ]);
        }

        $project = Project::create([
            'name'          => $request->name,
            'description'   => $request->description
        ]);

        return response()->json([
            'status'    => (bool)$project
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $project->name = $request['name'];
        $project->description = $request['description'];
        $project->save();

        return response()->json([
            'message'   => $project ? 'Success Updated Project' : 'Error Updating Project'
        ]);
    }

    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete;

        return response()->json([
            'status'    => (bool)$project
        ], 200);
    }
}
