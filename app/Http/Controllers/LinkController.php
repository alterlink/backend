<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use App\Models\AlterLink;
use Auth;

class LinkController extends Controller
{
    private $user;
    public function __contructor()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            return $next($request);
        }, ['except' => ['show']]);
    }

    public function index()
    {
        return response()->json(Link::all(), 200);
    }

    public function all()
    {
        return response()->json([
            Link::where('user_id', '=', $this->user)->get()
        ]);
    }

    public function show(Link $link)
    {
        return response()->json($link);
    }

    public function store(Request $request)
    {
        $link = Link::create([
            'link'            => $request->title,
            'url'             => $request->url,
            'alterlink_id'    => $request->alterlink_id,
            'user_id'         => $this->user
        ]);

        return response()->json([
            'status'    => (bool)$link
        ]);
    }

    public function update(Request $request, $id)
    {
        $link = Link::find($id);
        $link->title        = $request['title'];
        $link->url          = $request['url'];
        $link->alterlink_id = $request['alterink_id'];
        $link->save();

        return response()->json([
            'status'    => (bool)$link
        ]);
    }

    public function destroy($id)
    {
        $link = Link::findOrFail($id);
        $link->delete();

        return response()->json([
            'message'   => $link ? 'Success Deleted Link' : 'Error Deleting Link'
        ]);
    }
}
