<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('login', function (Request $request){
            $max = 4;
            $decay = 60;
            if(RateLimiter::tooManyAttempts($request->input(['email']) ?: $request->ip(), $max)){
                return response()->json([
                    'message'   => 'To Many Attemps'
                ], 429);
            }else{
                RateLimiter::hit($request->input(['email']) ?: $request->ip(), $decay);
            }
        });

        // rate limiter for email verirfication
        RateLimiter::for('verificationEmail', function (Request $request){
            $max = 1;
            $decay = 60;

            if(RateLimiter::tooManyAttempts($request->input(['email']), $max)){
                return response()->json([
                    'message'   => 'Your Email Will be Send',
                ]);
            }else{
                RateLimiter::hit($request->input(['email']), $decay);
            }
        });
    }
}
