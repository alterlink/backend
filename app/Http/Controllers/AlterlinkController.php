<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AlterLink;

class AlterlinkController extends Controller
{
    protected $user;
    public function __contructor()
    {
        $this->middelware(function ($request, $next){
            $this->user = Auth::user();
            
            return $next($request);
        })->except(['show']);
    }

    public function index()
    {
        if($this->user->hasPermissionTo('show all alterlink')){
            return response()->json(Alterlink::all(), 200);
        }else{
            return response()->json([
                'message'   => 'unauthorized'
            ], 401);
        }
    }

    /**
     * get all user alterlink list
     * verify user_id same with user id
     */
    public function all()
    {
        return response()->json(AlterLink::where('user_id', $user), 200);
    }

    /**
     * @param 
     */
    public function store(Request $request)
    {
        if($user->hasPermissionTo()){
            $alterlink = AlterLink::create([
                'title'     => $request->title,
                'user_id'   => $user
            ]);
        }

        return response()->json([
            'status'    => (bool)$alterlink,
            'message'   => $alterlink ? 'Success Created Alterlink Page' : 'Error Creating Alterlink Page'
        ]);
    }

    public function attachLink($id)
    {
        $alterlink = Alterlink::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $alterlink = AlterLink::find($id);
        $alterlink->title = $request['title'];
        $alterlink->slug  = $request['slug'];

        return response()->json([
            'status'    => (bool)$alterlink
        ]);

    }

    public function destroy($id)
    {
        $alterlink = AlterLink::findOrFail($id);
        $alterlink->delete;

        return response()->json([
            'status'    => (bool)$alterlink
        ]);
    }
}
