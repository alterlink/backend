<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;
use App\Models\User;
use Validator;
use Auth;

class RegisterController extends Controller
{

    public function __contructor()
    {
        //
    }

    public function Register(Request $request){
        $validation = Validator::make($request->all(),[
            'username'          => 'required|unique:users',
            'email'         => 'required|email',
            'password'      => ['required', Password::min(8)->mixedCase()->numbers()->letters()->symbols()],
            'c_password'    => 'required|same:password'
        ]); 
        if($validation->fails()){
            return response()->json([
                'error' => $validation->errors()
            ]);
        }

        $data   = $request->only('username', 'email','password');
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $user->assignRole('customer');

        return response()->json([
            'message'   => 'success register your account'
        ], 200);
    }
}
