<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'show all user']);
        Permission::create(['name' => 'store user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'show all project']);
        Permission::create(['name' => 'create project']);
        Permission::create(['name' => 'update project']);
        Permission::create(['name' => 'delete project']);
        Permission::create(['name' => 'show all alterlink']);
        Permission::create(['name' => 'create alterlink']);
        Permission::create(['name' => 'update alterlink']);
        Permission::create(['name' => 'delete alterlink']);
        Permission::create(['name' => 'show all link']);
        Permission::create(['name' => 'create link']);
        Permission::create(['name' => 'update link']);
        Permission::create(['name' => 'delete link']);
        Permission::create(['name' => 'attach link']);

        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo(Permission::all());

        $customer = Role::create(['name'    => 'customer']);
        $customer->givePermissionTo(['update user', 'show all project', 'create project', 'update project', 'delete project', 'show all alterlink','create alterlink', 'update alterlink', 'delete alterlink', 'show all link', 'create link', 'update link', 'delete link', 'attach link']);
        
        $silver = Role::create(['name'   => 'silver']);
    }
}