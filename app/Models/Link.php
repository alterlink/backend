<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Link extends Model
{
    use HasFactory, HasSlug;

    protected $fillable = [
        'title',
        'url',
        'alterlink_id',
        'user_id'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function AlterLink()
    {
        return $this->belongsTo(AlterLink::class);
    }
}
