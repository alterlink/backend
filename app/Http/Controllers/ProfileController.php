<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Auth;

class ProfileController extends Controller
{
    private $users;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->users = Auth::user();

            return $next($request);
        });
    }

    public function show()
    {
        return response()->json($this->user, 200);
    }

    public function update(Request $request)
    {
        $profile = Auth::user()->profile();
        $profile->first_name   = $request['first_name'];
        $profile->last_name    = $request['last_name'];
        $profile->avatar       = $request['avatar'];
        $profile->save();

        return response()->json([
            'status'    => (bool)$profile
        ]);
    }
}
