<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\TokenRepository;
use App\Models\User;
use Auth;

class LogoutController extends Controller
{
    private $users;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->users = Auth::user()->token()->id;

            return $next($request);
        });
    }
    
    public function logout()
    {
        $tokenRepository = app(TokenRepository::class);
        $tokenRepository->revokeAccessToken($this->users);

        return response()->json([
            'message'   => (bool)$tokenRepository
        ]);
    }
}
