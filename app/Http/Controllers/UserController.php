<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use validator;

class UserController extends Controller
{
    protected $user;
    public function __contruct()
    {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function index()
    {
        return response()->json();
    }

    public function show()
    {
        return response()->json([
            'user'  => $this->user
        ], 200);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $user = User::findOrFails($id);
        $user->delete();
    }
}
