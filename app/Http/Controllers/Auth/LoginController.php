<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use App\Models\User;
use Validator;
use Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username'  => 'required',
            'password'  => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'message'   => $validator->errors()
            ]);
        }

        $status = 401;
        $response = 'unauthorized';

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(Auth::attempt(array($fieldType => $request['username'], 'password' => $request['password']))){
            $status = 200;
            $response = [
                'user'  => Auth::user(),
                'accessToken'  => Auth::user()->createToken('alterlink')->accessToken
            ];
        }

        return response()->json($response, $status);
    }
}