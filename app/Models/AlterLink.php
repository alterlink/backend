<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class AlterLink extends Model
{
    use HasFactory,HasSlug;

    protected $fillable = [
        'title',
        'slug',
        'background',
        'user_id'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function Link()
    {
        //
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
