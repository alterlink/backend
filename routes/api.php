<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerifyEmail\VerifyEmailController;
use App\Http\Controllers\AlterlinkController;
use App\Http\Controllers\LinkController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('throttle:login')->group(function(){
    Route::post('/login', [LoginController::class, 'login']);
});

Route::post('/register', [RegisterController::class, 'register']);

Route::middleware('auth:api')->group(function(){
    Route::post('/logout', [LogoutController::class, 'logout']);
    Route::post('/profile/create', [ProfileController::class, 'create']);


    //alterlink route
});